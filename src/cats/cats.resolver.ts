import { Resolver, Query, Args, Mutation, Subscription } from '@nestjs/graphql';
import { CatsService } from './cats.service';
import { Cat } from '../graphql.schema';
import { PubSub } from 'graphql-subscriptions';
import { ParseIntPipe } from '@nestjs/common';
import { CreateCatDto } from './dto/create-cat.dto';

const pubSub = new PubSub()

@Resolver('Cats')
export class CatsResolver {
  constructor(private readonly catsService: CatsService) {}

  @Query()
  async getCatsList(
    @Args('take', ParseIntPipe) toTake: number,
    @Args('skip', ParseIntPipe) toSkip: number,
  ): Promise<Cat[]> {
    return await this.catsService.findAll(toTake, toSkip);
  }

  @Query('item')
  async findById(@Args('id', ParseIntPipe) id: number): Promise<Cat> {
    return await this.catsService.findOneById(id);
  }

  @Mutation('createCat')
  async create(@Args('createCatInput') cat: CreateCatDto): Promise<Cat> {
    const createdCat = await this.catsService.create(cat);
    pubSub.publish('catCreated', { catCreated: createdCat})
    return createdCat;
  }

  @Subscription('catCreated')
  catCreated() {
    return pubSub.asyncIterator('catCreated');
  }
}
