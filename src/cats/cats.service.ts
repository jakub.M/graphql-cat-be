import { Injectable } from '@nestjs/common';
import { Cat } from '../graphql.schema';
import { drop, take } from 'lodash';

@Injectable()
export class CatsService {
  private readonly cats: Cat[] = [{ id: 1, name: 'Cat', age: 5 }];

  create(cat: Cat): Cat {
    cat.id = this.cats.length + 1;
    this.cats.push(cat);
    return cat;
  }

  findAll(toTake: number, toSkip: number):any {
    let result = drop(this.cats, toSkip);
    result = take(result, toTake);
    return { data: result, count: this.cats.length };
  }

  findOneById(id: number): Cat {
    return this.cats.find(cat => cat.id === id);
  }
}
