import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { GraphQLModule } from '@nestjs/graphql'
import { CatsModule } from './cats/cats.module';
import { join } from 'path';
@Module({
  imports: [GraphQLModule.forRoot({
    definitions: {
      path: join(process.cwd(), '/src/graphql.schema.ts'),
      outputAs: 'class',
    },
    typePaths: ['./**/*.graphql'],
    installSubscriptionHandlers: true,
  }), CatsModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
